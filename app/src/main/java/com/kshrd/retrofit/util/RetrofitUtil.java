package com.kshrd.retrofit.util;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by pirang on 7/30/17.
 */

public class RetrofitUtil {

    public static RequestBody toRequestBody(String value){
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

}
