package com.kshrd.retrofit.api;

import com.kshrd.retrofit.entity.response.UserSignUpResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by pirang on 7/30/17.
 */

public interface UserService {

    @Multipart
    @POST("v1/api/users")
    Call<UserSignUpResponse> signUp(
            @Part("EMAIL") RequestBody email,
            @Part("PASSWORD") RequestBody password,
            @Part("NAME") RequestBody name,
            @Part("GENDER") RequestBody gender,
            @Part("TELEPHONE") RequestBody telephone,
            @Part("FACEBOOK_ID") RequestBody fbId,
            @Part MultipartBody.Part photo
    );

}
