package com.kshrd.retrofit.api;

import com.kshrd.retrofit.entity.form.SignInForm;
import com.kshrd.retrofit.entity.response.SignInResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by pirang on 7/29/17.
 */

public interface AuthenticationService {

    @POST("v1/api/authentication")
    Call<SignInResponse> signIn(@Body SignInForm signInForm);

}
