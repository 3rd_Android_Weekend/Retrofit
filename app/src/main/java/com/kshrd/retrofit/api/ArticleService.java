package com.kshrd.retrofit.api;

import com.kshrd.retrofit.entity.form.ArticleUpdateForm;
import com.kshrd.retrofit.entity.response.ArticleDeleteResponse;
import com.kshrd.retrofit.entity.response.ArticleResponse;
import com.kshrd.retrofit.entity.response.ArticleUpdateResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by pirang on 7/23/17.
 */

public interface ArticleService {

    @GET("v1/api/articles")
    Call<ArticleResponse> findArticle();

    @DELETE("v1/api/articles/{id}")
    Call<ArticleDeleteResponse> deleteArticle(@Path("id") int id);

    @PUT("v1/api/articles/{id}")
    Call<ArticleUpdateResponse> updateArticle(
            @Path("id") int id,
            @Body ArticleUpdateForm form
    );

}
