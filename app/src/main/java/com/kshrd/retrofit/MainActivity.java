package com.kshrd.retrofit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.kshrd.retrofit.activity.ArticleUpdateActivity;
import com.kshrd.retrofit.api.ArticleService;
import com.kshrd.retrofit.entity.Article;
import com.kshrd.retrofit.entity.response.ArticleDeleteResponse;
import com.kshrd.retrofit.entity.response.ArticleResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements RecyclerItemClickListener {

    private static final String TAG = "ooooo";
    private static final int UPDATE_ARTICLE = 1;
    private ArticleService articleService;
    private ArticleAdapter articleAdapter;
    private RecyclerView rvArticle;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");

        articleService = ServiceGenerator.createService(ArticleService.class);

        setupRecyclerView();
        findArticle();
    }

    private void initView() {
        rvArticle = (RecyclerView) findViewById(R.id.rvArticle);
    }

    private void setupRecyclerView() {
        rvArticle.setLayoutManager(new LinearLayoutManager(this));
        articleAdapter = new ArticleAdapter();
        articleAdapter.setRecyclerItemClickListener(this);
        rvArticle.setAdapter(articleAdapter);
    }

    private void findArticle() {
        Call<ArticleResponse> call = articleService.findArticle();
        call.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                ArticleResponse articleResponse = response.body();
                articleAdapter.addMoreItems(articleResponse.getArticleList());
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onItemClicked(int position) {

    }

    @Override
    public void onDeleteClicked(final int position) {
        final Article article = articleAdapter.findArticle(position);
        progressDialog.show();

        articleService.deleteArticle(article.getId())
            .enqueue(new Callback<ArticleDeleteResponse>() {
                @Override
                public void onResponse(Call<ArticleDeleteResponse> call, Response<ArticleDeleteResponse> response) {
                    ArticleDeleteResponse articleDeleteResponse = response.body();
                    if (response.isSuccessful()) {
                        if (articleDeleteResponse.getArticle() != null) { // Delete Successfully
                            articleAdapter.remove(position);
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ArticleDeleteResponse> call, Throwable t) {
                    t.printStackTrace();
                    progressDialog.dismiss();
                }
            });
    }

    @Override
    public void onEditClicked(int position) {
        Article article = articleAdapter.findArticle(position);
        Intent i = new Intent(this, ArticleUpdateActivity.class);
        i.putExtra("ARTICLE", article);
        startActivityForResult(i, UPDATE_ARTICLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPDATE_ARTICLE){
            if (resultCode == RESULT_OK){
                Article article = data.getExtras().getParcelable("ARTICLE");
                articleAdapter.updateItem(article);
            }
        }
    }
}
