package com.kshrd.retrofit.entity.form;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pirang on 7/29/17.
 */

public class SignInForm {

    @SerializedName("EMAIL")
    private String email;

    @SerializedName("PASSWORD")
    private String password;

    public SignInForm(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
