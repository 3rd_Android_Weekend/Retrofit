package com.kshrd.retrofit.entity.response;

import com.google.gson.annotations.SerializedName;
import com.kshrd.retrofit.entity.Article;
import com.kshrd.retrofit.entity.User;

/**
 * Created by pirang on 7/29/17.
 */

public class SignInResponse {

    @SerializedName("CODE")
    private String code;

    @SerializedName("MESSAGE")
    private String message;

    @SerializedName("DATA")
    private User user;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
