package com.kshrd.retrofit.entity.response;

import com.google.gson.annotations.SerializedName;
import com.kshrd.retrofit.entity.User;

/**
 * Created by pirang on 7/30/17.
 */

public class UserSignUpResponse {

    @SerializedName("CODE")
    private String code;

    @SerializedName("MESSAGE")
    private String message;

    @SerializedName("DATA")
    private User user;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
