package com.kshrd.retrofit.entity.response;

import com.google.gson.annotations.SerializedName;
import com.kshrd.retrofit.entity.Article;

/**
 * Created by pirang on 7/29/17.
 */

public class ArticleUpdateResponse {

    @SerializedName("CODE")
    private String code;

    @SerializedName("MESSAGE")
    private String message;

    @SerializedName("DATA")
    private Article article;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
