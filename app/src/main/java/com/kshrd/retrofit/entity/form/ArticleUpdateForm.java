package com.kshrd.retrofit.entity.form;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pirang on 7/29/17.
 */

public class ArticleUpdateForm {


    @SerializedName("TITLE")
    private String title;
    @SerializedName("DESCRIPTION")
    private String description;
    @SerializedName("AUTHOR")
    private int author;
    @SerializedName("CATEGORY_ID")
    private int categoryId;
    @SerializedName("STATUS")
    private String status;
    @SerializedName("IMAGE")
    private String image;

    public ArticleUpdateForm(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAuthor() {
        return author;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
