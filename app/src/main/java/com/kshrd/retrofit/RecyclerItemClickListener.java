package com.kshrd.retrofit;

/**
 * Created by pirang on 7/22/17.
 */

public interface RecyclerItemClickListener {

    void onItemClicked(int position);

    void onDeleteClicked(int position);

    void onEditClicked(int position);

}
