package com.kshrd.retrofit;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kshrd.retrofit.entity.Article;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pirang on 7/16/17.
 */

public class ArticleAdapter extends RecyclerView.Adapter<ArticleViewHolder> {

    private List<Article> articleList;
    private RecyclerItemClickListener recyclerItemClickListener;

    public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerItemClickListener = recyclerItemClickListener;
    }

    public ArticleAdapter() {
        articleList = new ArrayList<>();
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article, parent, false);
        return new ArticleViewHolder(view, recyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        holder.onBind(articleList.get(position));
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    public void addMoreItems(List<Article> articleList) {
        this.articleList.addAll(articleList);
        notifyDataSetChanged();
    }

    public void clear() {
        this.articleList.clear();
    }

    public Article findArticle(int position) {
        return this.articleList.get(position);
    }

    public void remove(int position){
        this.articleList.remove(position);
        notifyItemRemoved(position);
    }

    public int getPosition(int articleId) {
        for (Article article : articleList){
            if (article.getId() == articleId){
                return articleList.indexOf(article);
            }
        }
        return -1;
    }

    public void updateItem(Article article){
        int position = getPosition(article.getId());
        Article temp = findArticle(position);
        temp.setTitle(article.getTitle());
        notifyItemChanged(position);
    }

}
