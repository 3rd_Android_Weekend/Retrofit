package com.kshrd.retrofit;

import android.media.Image;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kshrd.retrofit.entity.Article;

/**
 * Created by pirang on 7/16/17.
 */

public class ArticleViewHolder extends ViewHolder implements View.OnClickListener {

    TextView tvTitle;
    TextView tvDescription;
    ImageView ivDelete;
    ImageView ivEdit;

    private RecyclerItemClickListener recyclerItemClickListener;

    public ArticleViewHolder(View itemView, RecyclerItemClickListener recyclerItemClickListener) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
        ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
        ivEdit = (ImageView) itemView.findViewById(R.id.ivEdit);
        this.recyclerItemClickListener = recyclerItemClickListener;

        ivDelete.setOnClickListener(this);
        ivEdit.setOnClickListener(this);
    }

    public void onBind(Article article){
        tvTitle.setText(article.getTitle());
        tvDescription.setText(article.getDescription());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivDelete:
                recyclerItemClickListener.onDeleteClicked(getAdapterPosition());
                break;
            case R.id.ivEdit:
                recyclerItemClickListener.onEditClicked(getAdapterPosition());
                break;
        }

    }
}
