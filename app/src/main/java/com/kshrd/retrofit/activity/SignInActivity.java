package com.kshrd.retrofit.activity;

import android.app.Service;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kshrd.retrofit.MainActivity;
import com.kshrd.retrofit.R;
import com.kshrd.retrofit.ServiceGenerator;
import com.kshrd.retrofit.api.AuthenticationService;
import com.kshrd.retrofit.entity.form.SignInForm;
import com.kshrd.retrofit.entity.response.SignInResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    private EditText etEmail;
    private EditText etPassword;
    private Button btnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        initView();

        final AuthenticationService authenticationService = ServiceGenerator.createService(AuthenticationService.class);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                authenticationService
                    .signIn(new SignInForm(email, password))
                    .enqueue(new Callback<SignInResponse>() {
                        @Override
                        public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                            SignInResponse signInResponse = response.body();
                            if (response.isSuccessful()){
                                if (signInResponse.getUser() != null){  // Sign In Successfully
                                    startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                    finish();
                                } else {
                                    Toast.makeText(SignInActivity.this, "Invalid Information", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<SignInResponse> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
            }
        });
    }

    private void initView() {
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
    }
}
