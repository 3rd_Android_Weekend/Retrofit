package com.kshrd.retrofit.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kshrd.retrofit.R;
import com.kshrd.retrofit.ServiceGenerator;
import com.kshrd.retrofit.api.UserService;
import com.kshrd.retrofit.entity.response.UserSignUpResponse;
import com.kshrd.retrofit.util.RetrofitUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements Validator.ValidationListener {

    private static final int OPEN_GALLERY = 1;
    private static final int READ_EXTERNAL_STORAGE_PERMISSION = 1;

    @Email(messageResId = R.string.error_email)
    private EditText etEmail;

    @Password(min = 10)
    private EditText etPassword;

    @ConfirmPassword
    private EditText etConfirmPassword;

    private Button btnSignUp;
    private Button btnBrowse;
    private Validator validator;
    private String imagePath;

    private UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initView();

        userService = ServiceGenerator.createService(UserService.class);
        validator = new Validator(this);
        validator.setValidationListener(this);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });

        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    int checkPermission = ContextCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
                    if (checkPermission == PackageManager.PERMISSION_DENIED){
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_PERMISSION);
                    } else {
                        browseGallery();
                    }
                } else {
                    browseGallery();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_EXTERNAL_STORAGE_PERMISSION){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                browseGallery();
            }
        }
    }

    private void browseGallery() {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.setType("image/*");
        startActivityForResult(i, OPEN_GALLERY);
    }

    private void initView() {
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        btnBrowse = (Button) findViewById(R.id.btnBrowse);

    }

    @Override
    public void onValidationSucceeded() {
        signUp();
    }

    private void signUp() {
        RequestBody email = RetrofitUtil.toRequestBody(etEmail.getText().toString());
        RequestBody password = RetrofitUtil.toRequestBody(etPassword.getText().toString());
        RequestBody name = RetrofitUtil.toRequestBody("Annonymous");
        RequestBody telephone = RetrofitUtil.toRequestBody("012345678");
        RequestBody gender = RetrofitUtil.toRequestBody("m");
        RequestBody fbId = RetrofitUtil.toRequestBody("1");

        File file = new File(imagePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part photo = MultipartBody.Part.createFormData("PHOTO", file.getName(), requestBody);

        userService.signUp(
            email,
                password,
                name,
                gender,
                telephone,
                fbId,
                photo
        ).enqueue(new Callback<UserSignUpResponse>() {
            @Override
            public void onResponse(Call<UserSignUpResponse> call, Response<UserSignUpResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getUser() != null){
                        startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
                        finish();
                    } else {
                        Toast.makeText(SignUpActivity.this, "SignUp Failed successfully", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserSignUpResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OPEN_GALLERY){
            if (resultCode == RESULT_OK){
                Uri uri = data.getData();
                String[] column = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, column, null, null, null);
                cursor.moveToFirst();
                imagePath = cursor.getString(cursor.getColumnIndex(column[0]));
                cursor.close();
            }
        }
    }
}
