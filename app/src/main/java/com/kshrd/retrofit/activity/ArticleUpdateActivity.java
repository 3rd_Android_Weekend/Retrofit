package com.kshrd.retrofit.activity;

import android.app.Service;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kshrd.retrofit.R;
import com.kshrd.retrofit.ServiceGenerator;
import com.kshrd.retrofit.api.ArticleService;
import com.kshrd.retrofit.entity.Article;
import com.kshrd.retrofit.entity.form.ArticleUpdateForm;
import com.kshrd.retrofit.entity.response.ArticleUpdateResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleUpdateActivity extends AppCompatActivity {

    private EditText etTitle;
    private Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_update);

        etTitle = (EditText) findViewById(R.id.etTitle);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);

        final ArticleService articleService = ServiceGenerator.createService(ArticleService.class);

        final Article article = getIntent().getExtras().getParcelable("ARTICLE");
        etTitle.setText(article.getTitle());

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                article.setTitle(etTitle.getText().toString());
                articleService
                        .updateArticle(article.getId(), new ArticleUpdateForm(article.getTitle()))
                        .enqueue(new Callback<ArticleUpdateResponse>() {
                            @Override
                            public void onResponse(Call<ArticleUpdateResponse> call, Response<ArticleUpdateResponse> response) {
                                if (response.isSuccessful()){
                                    if (response.body().getArticle() != null){  // Update Successfully
                                        Intent i = new Intent();
                                        i.putExtra("ARTICLE", article);
                                        setResult(RESULT_OK, i);
                                        finish();
                                    } else {
                                        Toast.makeText(ArticleUpdateActivity.this, "Update Failed", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ArticleUpdateResponse> call, Throwable t) {
                                Toast.makeText(ArticleUpdateActivity.this, "Request Failed", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }


}
